import Vue from "vue";
import * as VueFire from "vuefire";
import App from "./App";
import router from "./router";

Vue.use(VueFire);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
